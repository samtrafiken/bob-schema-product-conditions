# BoB schema product-conditions

It is RECOMMENDED to use the categories specified in [product-conditions.json](https://bitbucket.org/samtrafiken/bob-schema-product-conditions/src/BOBX-53-add-product-conditions/product-conditions.json) for the category property in the productCondition data structure defined in the [BoB Product API](https://samtrafiken.atlassian.net/wiki/spaces/BOB/pages/2042462293/BoB+Product+v3+OpenAPI). For example texts, see the [Example product conditions wiki page](https://samtrafiken.atlassian.net/wiki/spaces/BOB/pages/2995159075/Example+product+conditions+draft).

This is a part of the [BoB - National Ticket and Payment Standard](https://bob.samtrafiken.se), hosted by Samtrafiken i Sverige AB
